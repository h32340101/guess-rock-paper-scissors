package com.example.guess_rock_paper_scissors

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.concurrent.schedule

class MainActivity : AppCompatActivity() {
    private val fingerButtons by lazy {
        arrayOf(rockButton, paperButton, scissorsButton, resetButton)
    }
    private val fingerImageViews by lazy {
        arrayOf(playerFingerImageView, computerFingerImageView)
    }
    private var round = 0
    private var win = 0
    private var draw = 0
    private var lose = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fingerImageViews.forEach { it.visibility = View.INVISIBLE }
        initListener()
    }

    private fun initListener() {
        rockButton.setOnClickListener(onFingerClickListener)
        paperButton.setOnClickListener(onFingerClickListener)
        scissorsButton.setOnClickListener(onFingerClickListener)
        resetButton.setOnClickListener {
            round = 0; win = 0; draw = 0; lose = 0
            "round: $round, win: $win, draw: $draw, lose: $lose".also { recordTextView.text = it }
        }
    }

    private val onFingerClickListener = View.OnClickListener { v ->
        val player = when (v.id) {
            R.id.rockButton -> R.drawable.rock
            R.id.paperButton -> R.drawable.paper
            R.id.scissorsButton -> R.drawable.scissor
            else -> null
        } ?: return@OnClickListener

        val computer = arrayOf(
            R.drawable.rock,
            R.drawable.paper,
            R.drawable.scissor
        ).random()

        val fingerMap = hashMapOf(
            R.drawable.rock to Finger.ROCK,
            R.drawable.paper to Finger.PAPER,
            R.drawable.scissor to Finger.SCISSORS
        )
        compare(fingerMap[player]!!, fingerMap[computer]!!).also {
            when {
                it > 0 -> win++
                it < 0 -> lose++
                else -> draw++
            }
            round++
        }
        "round: $round, win: $win, draw: $draw, lose: $lose".also { recordTextView.text = it }

        playerFingerImageView
            .setImageDrawable(ContextCompat.getDrawable(this@MainActivity, player))
        computerFingerImageView
            .setImageDrawable(ContextCompat.getDrawable(this@MainActivity, computer))

        fingerButtons.forEach { it.isClickable = false }
        fingerImageViews.forEach { it.visibility = View.VISIBLE }

        Timer("delay between round", true).schedule(1500) {
            runOnUiThread {
                fingerButtons.forEach { it.isClickable = true }
                fingerImageViews.forEach { it.visibility = View.INVISIBLE }
            }
        }
    }

    private fun compare(player: Finger, computer: Finger): Int {
        val classify = ((player.ordinal + 3) - computer.ordinal) % 3
        val turn = (classify + 1) % 3
        val result = turn - 1
        Log.d(TAG, result.toString())
        return result
    }

    enum class Finger { ROCK, PAPER, SCISSORS }

    companion object {
        private const val TAG = "MainActivity"
    }
}